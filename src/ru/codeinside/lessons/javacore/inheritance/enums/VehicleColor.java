package ru.codeinside.lessons.javacore.inheritance.enums;

import java.util.Locale;

/**
 * Перечисление описывает возможные цвета автомобилей.
 */
public enum VehicleColor {
    RED("red"),
    BLUE("blue"),
    GREEN("green"),
    BLACK("black"),
    WHITE("white"),
    LIGHT_GREEN("light green"),
    UNDEFINED("undefined");

    private String color;

    VehicleColor(String color) {
        this.color = color;
    }

    /**
     * Метод возвращает объект {@link VehicleColor} на основе переданного названия цвета.
     *
     * @param color {@link String} название цвета
     * @return {@link VehicleColor}
     */
    public static VehicleColor getColorByName(String color) {
        try {
            return VehicleColor.valueOf(color.replaceAll(" ", "_").toUpperCase(Locale.ROOT));
        } catch (IllegalArgumentException exception) {
            return VehicleColor.UNDEFINED;
        }
    }
}
