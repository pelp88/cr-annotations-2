package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

import java.util.Random;
import java.util.stream.Collectors;

public abstract class Vehicle {

    public String vin = generateVin();
    public int yearOfProduction;
    public VehicleColor color;

    protected Vehicle(int year, String color) {
        setProductionYear(year);
        setColor(color);
    }

    protected Vehicle(int year, VehicleColor color) {
        setProductionYear(year);
        this.color = color;
    }

    public abstract String getVehicleInfo();

    private static String generateVin() {
        var symbols = "ABCDEFGHJKLMNPQRSTUVWXYZ1234567890".chars()
                .mapToObj(sym -> (char) sym).toArray();
        return new Random().ints(17, 0, 33)
                .mapToObj(elem -> symbols[elem].toString()).collect(Collectors.joining());
    }

    public void setProductionYear(Integer year) {
        this.yearOfProduction = year;
    }

    public void setColor(String color) {
        this.color = VehicleColor.getColorByName(color);
    }

    public VehicleColor getColor() {
        return this.color;
    }

    public int getYearOfProduction() {
        return this.yearOfProduction;
    }
}
