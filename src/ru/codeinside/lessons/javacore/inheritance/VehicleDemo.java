package ru.codeinside.lessons.javacore.inheritance;

public class VehicleDemo {
    public static void main(String[] args) {
        Car car = new Car(2010, "light green", true, false);
        Truck truck = new Truck(1992, "cyan", false);

        System.out.println(car.getVehicleInfo());
        System.out.println(truck.getVehicleInfo());

        var carClass = Car.class;

        System.out.println("Reflexion:");
        System.out.println("Name of class: " + carClass.getName());
        System.out.println("Name of parent: " + carClass.getSuperclass().getName());
        System.out.println("Class constructors: " + carClass.getConstructor());
        for (var field : carClass.getFields()) {
            System.out.println("Field: " field);
            System.out.println("Modifiers of field: " + field.getModifiers());
        }
        for (var method : carClass.getDeclaredMethods()) {
            System.out.println("Method: " + method);
            System.out.println("Modifiers of method: " + method.getModifiers());
        }
    }
}
